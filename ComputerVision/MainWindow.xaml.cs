﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ComputerVision
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    #region Private Fields

    private readonly MainWindowViewModel _viewModel;
    private HSV _hsv;

    #endregion Private Fields

    #region Public Constructors

    public MainWindow()
    {
      InitializeComponent();
      _viewModel = new MainWindowViewModel();
      DataContext = _viewModel;
    }

    #endregion Public Constructors

    #region Public Methods

    public void HSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      var h = e.NewValue;

      ((Slider) sender).SelectionEnd = h;

      _hsv.H = h;

      _viewModel.ChangeHSV(_hsv);
    }

    public void SSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      var s = e.NewValue;

      ((Slider) sender).SelectionEnd = s;

      _hsv.S = s;

      _viewModel.ChangeHSV(_hsv);
    }

    public void VSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      var v = e.NewValue;

      ((Slider) sender).SelectionEnd = v;

      _hsv.V = v;

      _viewModel.ChangeHSV(_hsv);
    }

    #endregion Public Methods

    #region Protected Methods

    protected override void OnMouseMove(MouseEventArgs e)
    {
      POINTAPI point;
      GetCursorPos(out point);
      Point pos = new Point(point.X, point.Y);

      RGB rgb = GetPixelRGB(pos);

      _viewModel.ChangeHighlightPixelInfo(rgb);
    }

    #endregion Protected Methods

    #region Private Methods

    [System.Runtime.InteropServices.DllImport("user32")]
    private static extern int GetCursorPos(out POINTAPI lpPoint);

    [System.Runtime.InteropServices.DllImport("gdi32")]
    private static extern int GetPixel(int hdc, int nXPos, int nYPos);

    private static RGB GetPixelRGB(Point point)
    {
      int lDC = GetWindowDC(0);
      int intColor = GetPixel(lDC, (int) point.X, (int) point.Y);

      ReleaseDC(0, lDC);

      //byte a = (byte)( ( intColor >> 0x18 ) & 0xffL );
      byte b = (byte) ((intColor >> 0x10) & 0xffL);
      byte g = (byte) ((intColor >> 8) & 0xffL);
      byte r = (byte) (intColor & 0xffL);
      return new RGB { R = r, G = g, B = b };
    }

    [System.Runtime.InteropServices.DllImport("user32")]
    private static extern int GetWindowDC(int hwnd);

    [System.Runtime.InteropServices.DllImport("user32")]
    private static extern int ReleaseDC(int hWnd, int hDC);

    private void LoadImageButtonClick(object sender, RoutedEventArgs e)
    {
      _viewModel.LoadImage();
    }

    #endregion Private Methods

    #region Private Structs

    private struct POINTAPI
    {
      #region Public Fields

      public uint X;
      public uint Y;

      #endregion Public Fields
    }

    #endregion Private Structs
  }
}
