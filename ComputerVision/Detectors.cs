﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ComputerVision
{
  class Detectors
  {
    private static int width;
    private static int height;

    public static uint[,] Fertness(int width, int height, uint[,] pixels)
    {
      Detectors.width = width;
      Detectors.height = height;

      Filters.SobelFilterExecute(width, height, pixels);

      var Ix = Filters._gSobelX;
      var Iy = Filters._gSobelY;

      var ixx = MultiplyMatrix(Ix, Ix);
      var iyy = MultiplyMatrix(Iy, Iy);
      var ixy = MultiplyMatrix(Ix, Iy);

      ixx = Filters.GaussianFilterExecute(width, height, ixx, 2);
      iyy = Filters.GaussianFilterExecute(width, height, iyy, 2);
      ixy = Filters.GaussianFilterExecute(width, height, ixy, 2);

      var newImage = new uint[height, width];

      var temp = new double[height, width];

      var rMax = 0.0;

      for (var i = 0; i < height - 1; i++)
      {
        for (var j = 0; j < width-1; j++)
        {
          var m = new uint[2, 2];
          m[0, 0] = ixx[i, j];
          m[1, 0] = ixy[i, j];
          m[0, 1] = ixy[i, j];
          m[1, 1] = iyy[i, j];

          var det = Det(m);
          var trace = Trace(m);

          temp[i, j] = det * 1.0 / trace * 1.0;

          if (temp[i, j] > rMax)
          {
            rMax = temp[i, j];
          }
        }
      }

      var white = Filters.RGB2Pixel(new RGB_f {R = 255, G = 255, B = 255});

      for (int i = 1; i < height - 2; i++)
      {
        for (int j = 1; j < width-2; j++)
        {
          if (temp[i, j] > 0.01 * rMax
              && temp[i, j] > temp[i - 1, j - 1]
              && temp[i, j] > temp[i - 1, j + 1]
              && temp[i, j] > temp[i + 1, j - 1]
              && temp[i, j] > temp[i + 1, j + 1])
          {
            newImage[i, j] = white;
          }
        }
      }

      return newImage;
    }

    private static double Trace(uint[,] m)
    {
      return m[0, 0] + m[1, 1];
    }
    private static double Det(uint[,] m)
    {
      return m[0, 0] * m[1, 1] - m[0, 1] * m[1, 0];
    }

    private static uint[,] MultiplyMatrix(uint[,] matrix1, uint[,] matrix2)
    {
      var mult = new uint[height, width];

      for (var i = 0; i < height - 1; i++)
      {
        for (var j = 0; j < width - 1; j++)
        {
          var rgb1 = Filters.Pixel2RGB(matrix1[i, j]);
          var rgb2 = Filters.Pixel2RGB(matrix2[i, j]);

          mult[i, j] = Filters.RGB2Pixel(new RGB_f() { R = rgb1.R * rgb2.R, G = rgb1.G * rgb2.G , B = rgb1.B * rgb2.B });
        }
      }

      return mult;
    }
  }
}
