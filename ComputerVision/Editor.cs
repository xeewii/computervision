﻿using System;

namespace ComputerVision
{
  internal class Editor
  {
    #region Public Methods

    public static RGB ChangeHSV(RGB rgb, HSV hsv)
    {
      var oldHsv = Converter.RGB2HSV(rgb);

      var newHsv = new HSV
      {
        H = GetValid((Math.Abs(hsv.H) + oldHsv.H) % 360, 360),
        S = GetValid(oldHsv.S + hsv.S, 100),
        V = GetValid(oldHsv.V + hsv.V, 100)
      };

      return Converter.HSV2RGB(newHsv);
    }

    public static YIQ ChangeHue(YIQ yiq, int h)
    {
      var tao = h * 0.0174533;
      var u = Math.Cos(tao);
      var w = Math.Sin(tao);

      return new YIQ
      {
        Y = yiq.Y,
        I = yiq.I * u - yiq.Q * w,
        Q = yiq.I * w + yiq.Q * u
      };
    }

    public static YIQ ChangeSaturation(YIQ yiq, int s)
    {
      var yiq_new = new YIQ
      {
        Y = yiq.Y,
        I = yiq.I * s,
        Q = yiq.Q * s
      };

      return yiq_new;
    }

    public static YIQ ChangeValue(YIQ yiq, int v)
    {
      var yiq_new = new YIQ
      {
        Y = yiq.Y * v,
        I = yiq.I * v,
        Q = yiq.Q * v
      };

      return yiq_new;
    }

    public static double GetValid(double val, int max, int min = 0)
    {
      if (val > max)
      {
        return max;
      }

      if (val < min)
      {
        return min;
      }

      return val;
    }

    #endregion Public Methods
  }
}
