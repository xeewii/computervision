﻿using System;
using System.Collections.Generic;

namespace ComputerVision
{
  internal class SplitAndMergeSegmentation
  {
    #region Private Fields

    private const double Sigma = 200;
    private static uint[] _blue;
    private static uint[] _green;
    private static int _height;
    private static uint[,] _pixels;
    private static uint[] _red;
    private static int _width;

    #endregion Private Fields

    #region Public Methods

    public static uint[,] Execute(int width, int height, uint[,] pixels)
    {
      _height = height;
      _width = width;
      _pixels = pixels;

      var length = _height * _width;
      _red = new uint[length];
      _green = new uint[length];
      _blue = new uint[length];

      for (var i = 0; i < _height - 1; i++)
      {
        for (var j = 0; j < _width; j++)
        {
          var rgb = Filters.Pixel2RGB(_pixels[i, j]);

          _red[i * (_height - 1) + j] = (uint) rgb.R;
          _green[i * (_height - 1) + j] = (uint) rgb.G;
          _blue[i * (_height - 1) + j] = (uint) rgb.B;
        }
      }

      var parent = new Node(0, 0, width, height);

      var nodes = new LinkedList<Node>();
      nodes.AddFirst(parent);
      while (nodes.Count > 0)
      {
        var node = nodes.First.Value;
        nodes.RemoveFirst();
        if (IsCongeneric(node))
        {
          continue;
        }

        var node1 = new Node(node.X, node.Y, node.Width / 2, node.Height / 2);
        var node2 = new Node(node.X + node.Width / 2, node.Y, node.Width - node.Width / 2, node.Height / 2);
        var node3 = new Node(node.X, node.Y + node.Height / 2, node.Width / 2, node.Height - node.Height / 2);
        var node4 = new Node(node.X + node.Width / 2, node.Y + node.Height / 2, node.Width - node.Width / 2, node.Height - node.Height / 2);

        node.Children = new[] { node1, node2, node3, node4 };

        nodes.AddLast(node1);
        nodes.AddLast(node2);
        nodes.AddLast(node3);
        nodes.AddLast(node4);
      }

      FillNodes(parent);

      var newPixels = new uint[height, width];
      for (var i = 0; i < height - 1; i++)
      {
        for (var j = 0; j < width; j++)
        {
          newPixels[i, j] = Filters.RGB2Pixel(new RGB_f { R = _red[i * (height - 1) + j], G = _green[i * (height - 1) + j], B = _blue[i * (height - 1) + j] });
        }
      }

      return newPixels;
    }

    #endregion Public Methods

    #region Private Methods

    private static void FillNodes(Node node)
    {
      if (node.Children == null)
      {
        var maxHeight = node.Y + node.Height;
        var minHeight = node.Y;
        var maxWidth = node.X + node.Width;
        var minWidth = node.X;

        double redMean = 0;
        double greenMean = 0;
        double blueMean = 0;

        for (var y = minHeight; y < maxHeight; y++)
        {
          for (var x = minWidth; x < maxWidth; x++)
          {
            redMean += _red[y * _width + x];
            greenMean += _green[y * _width + x];
            blueMean += _blue[y * _width + x];
          }
        }

        redMean /= node.Width * node.Height;
        greenMean /= node.Width * node.Height;
        blueMean /= node.Width * node.Height;

        for (var y = minHeight; y < maxHeight; y++)
        {
          for (var x = minWidth; x < maxWidth; x++)
          {
            _red[y * _width + x] = (uint) redMean;
            _green[y * _width + x] = (uint) greenMean;
            _blue[y * _width + x] = (uint) blueMean;
          }
        }
      }
      else
      {
        foreach (var child in node.Children)
        {
          FillNodes(child);
        }
      }
    }

    private static bool IsCongeneric(Node node)
    {
      if (Math.Max(node.Height, node.Width) < 4)
      {
        return true;
      }

      var maxHeight = node.Y + node.Height;
      var minHeight = node.Y;
      var maxWidth = node.X + node.Width;
      var minWidth = node.X;

      double mean = 0;
      double sigma = 0;

      for (var y = minHeight; y < maxHeight; y++)
      {
        for (var x = minWidth; x < maxWidth; x++)
        {
          mean += _red[y * _width + x] * 0.2126 + _green[y * _width + x] * 0.7152 + _blue[y * _width + x] * 0.0722;
        }
      }

      mean /= node.Width * node.Height;

      for (var y = minHeight; y < maxHeight; y++)
      {
        for (var x = minWidth; x < maxWidth; x++)
        {
          var v = _red[y * _width + x] * 0.2126 + _green[y * _width + x] * 0.7152 + _blue[y * _width + x] * 0.0722 - mean;
          sigma += v * v;
        }
      }

      sigma /= node.Width * node.Height - 1;
      return sigma < Sigma;
    }

    #endregion Private Methods

    #region Private Classes

    private class Node
    {
      #region Public Fields

      public readonly int Height;
      public readonly int Width;
      public readonly int X;
      public readonly int Y;
      public Node[] Children;

      #endregion Public Fields

      #region Public Constructors

      public Node(int x, int y, int w, int h)
      {
        X = x;
        Y = y;
        Width = w;
        Height = h;
      }

      #endregion Public Constructors
    }

    #endregion Private Classes
  }
}
