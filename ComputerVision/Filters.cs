﻿using System;

namespace ComputerVision
{
  public class Filters
  {
    #region Private Fields

    private static int[,] _angleSobel;
    private static double[,] _gSobel;
    public static uint[,] _gSobelX;
    public static uint[,] _gSobelY;
    private static uint[,] _originalPixels;

    #endregion Private Fields

    #region Public Methods

    public static RGB_f CalculateNewColor(uint pixel, double coefficient)
    {
      return new RGB_f
      {
        R = (float) (coefficient * ((pixel & 0x00FF0000) >> 16)),
        G = (float) (coefficient * ((pixel & 0x0000FF00) >> 8)),
        B = (float) (coefficient * (pixel & 0x000000FF))
      };
    }

    public static uint[,] GaborFilterExecute(int width, int height, uint[,] pixels)
    {
      int n = 10;
      double[,] matrix = new double[n, n];

      var lambda = 2;
      var sigma = 0.56 * lambda;
      var fi = 0;
      var gamma = 0.1;
      var F = 1 / lambda;
      var tetha = 135;

      var sigma2 = sigma * sigma;
      var gamma2 = gamma * gamma;

      var d = -0.5 / sigma2;
      var a = 2 * Math.PI * F;

      for (int i = 0; i < n; i++)
      {
        for (int j = 0; j < n; j++)
        {
          var x = i * Math.Cos(tetha) + j * Math.Sin(tetha);
          var y = -i * Math.Sin(tetha) + j * Math.Cos(tetha);

          matrix[i, j] = Math.Exp(d * (x * x + y * y * gamma2)) * Math.Cos(a * x + fi);
        }
      }

      NormalizedMatrix(matrix, n);

      return MatrixFiltration(width, height, pixels, n, matrix);
    }

    public static uint[,] GaussianFilterExecute(int width, int height, uint[,] pixels, int sigma)
    {
      int n = 5;
      double[,] matrix = new double[n, n];

      int sigma2 = sigma * sigma;

      for (int i = 0; i < n; i++)
      {
        for (int j = 0; j < n; j++)
        {
          matrix[i, j] = 0.159155 / sigma2 * Math.Exp(-(i * i + j * j) / (2.0 * sigma2));
        }
      }

      NormalizedMatrix(matrix, n);

      return MatrixFiltration(width, height, pixels, n, matrix);
    }

    public static uint[,] KannyFilterExecute(int width, int height, uint[,] pixels)
    {
      _originalPixels = pixels;

      var gaussianPixels = GaussianFilterExecute(width, height, pixels, 1);

      var sobelPixels = SobelFilterExecute(width, height, gaussianPixels);

      var nonMaximumPixels = SuppressionOfNonMaximum(width, height, sobelPixels);

      var newPixels = BindingEdges(width, height, nonMaximumPixels);

      return newPixels;
    }

    public static uint[,] MatrixFiltration(int width, int height, uint[,] pixels, int N, double[,] matrix)
    {
      int gap = N / 2;
      int tmpHeight = height + 2 * gap;
      int tmpWidth = width + 2 * gap;
      uint[,] newPixels = new uint[height, width];

      uint[,] tmpPixels = GetExpandedPixelArray(pixels, gap, width, height, tmpWidth, tmpHeight);

      //применение ядра свертки
      for (int i = gap; i < tmpHeight - gap; i++)
        for (int j = gap; j < tmpWidth - gap; j++)
        {
          var newPixel = GetNewPixel(tmpPixels, matrix, N, gap, i, j);

          ValidatePixelColors(newPixel);

          newPixels[i - gap, j - gap] = RGB2Pixel(newPixel);
        }

      return newPixels;
    }

    public static uint[,] OtsuFilterExecute(int width, int height, uint[,] pixels)
    {
      var histData = new int[256];

      for (var i = 0; i < height; i++)
      {
        for (var j = 0; j < width; j++)
        {
          var h = 0xFF & pixels[i, j];
          histData[h]++;
        }
      }

      // Total number of pixels
      var total = width * height;

      float sum = 0;
      for (var t = 0; t < 256; t++)
      {
        sum += t * histData[t];
      }

      float sumB = 0;
      var wB = 0;

      float varMax = 0;
      var threshold = 0;

      for (var t = 0; t < 256; t++)
      {
        wB += histData[t];
        if (wB == 0)
          continue;

        var wF = total - wB;
        if (wF == 0)
          break;

        sumB += t * histData[t];

        var mB = sumB / wB;
        var mF = (sum - sumB) / wF;

        var varBetween = (float) wB * wF * (mB - mF) * (mB - mF);

        if (varBetween > varMax)
        {
          varMax = varBetween;
          threshold = t;
        }
      }

      var newPixels = new uint[height, width];

      var white = RGB2Pixel(new RGB_f { R = 255, G = 255, B = 255 });
      var black = RGB2Pixel(new RGB_f { R = 0, G = 0, B = 0 });

      for (var i = 0; i < height; i++)
      {
        for (var j = 0; j < width; j++)
        {
          newPixels[i, j] = (0xFF & pixels[i, j]) > threshold ? white : black;
        }
      }

      return newPixels;
    }

    public static RGB_f Pixel2RGB(uint pixel)
    {
      return new RGB_f
      {
        R = (float) ((pixel & 0x00FF0000) >> 16),
        G = (float) ((pixel & 0x0000FF00) >> 8),
        B = (float) (pixel & 0x000000FF)
      };
    }

    //сборка каналов
    public static uint RGB2Pixel(RGB_f ColorOfPixel)
    {
      return 0xFF000000 | ((uint) ColorOfPixel.R << 16) | ((uint) ColorOfPixel.G << 8) | ((uint) ColorOfPixel.B);
    }

    public static uint[,] SobelFilterExecute(int width, int height, uint[,] pixels)
    {
      const int N = 3;
      double[,] gXMatrix = new double[N, N] { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
      double[,] gYMatrix = new double[N, N] { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };
      int gap = N / 2;
      int tmpHeight = height + 2 * gap;
      int tmpWidth = width + 2 * gap;
      uint[,] newPixels = new uint[height, width];

      uint[,] tmpPixels = GetExpandedPixelArray(pixels, gap, width, height, tmpWidth, tmpHeight);

      _gSobel = new double[tmpHeight - gap, tmpWidth - gap];
      _angleSobel = new int[tmpHeight - gap, tmpWidth - gap];
      _gSobelX = new uint[height, width];
      _gSobelY = new uint[height, width];

      //применение ядра свертки
      for (int i = gap; i < tmpHeight - gap; i++)
        for (int j = gap; j < tmpWidth - gap; j++)
        {
          var newPixelX = GetNewPixel(tmpPixels, gXMatrix, N, gap, i, j);
          var newPixelY = GetNewPixel(tmpPixels, gYMatrix, N, gap, i, j);

          var r = (newPixelX.R * newPixelX.R + newPixelY.R * newPixelY.R);
          var g = (newPixelX.G * newPixelX.G + newPixelY.G * newPixelY.G);
          var b = (newPixelX.B * newPixelX.B + newPixelY.B * newPixelY.B);

          var gX = RGB2Pixel(newPixelX);
          var gY = RGB2Pixel(newPixelY);

          _gSobel[i, j] = Math.Sqrt(gX * gX + gY * gY);
          _angleSobel[i, j] = (int) (Math.Atan(gY / gX) * 100);

          if (i < height && j < width)
          {
            _gSobelX[i, j] = gX;
            _gSobelY[i, j] = gY;
          }

          var limit = 128 * 128;
          if (!(r > limit || g > limit || b > limit))
          {
            r = 0;
            g = 0;
            b = 0;
          }
          else
          {
            r = 255;
            g = 255;
            b = 255;
          }

          var newPixel = new RGB_f { R = (float) r, G = (float) g, B = (float) b };

          ValidatePixelColors(newPixel);

          newPixels[i - gap, j - gap] = RGB2Pixel(newPixel);
        }

      return newPixels;
    }

    public static uint[,] SuppressionOfNonMaximum(int width, int height, uint[,] pixels)
    {
      var newPixels = new uint[height, width];

      for (int i = 1; i < height - 1; i++)
      {
        for (int j = 1; j < width - 1; j++)
        {
          var tetha = _angleSobel[i, j];

          if (tetha < 0)
          {
            tetha += 180;
          }

          var pixel = pixels[i, j];

          double val1 = 0, val2 = 0;
          if (tetha >= 0 && tetha < 22.5 || tetha >= 157.5 && tetha <= 180)
          {
            val1 = _gSobel[i, j + 1];
            val2 = _gSobel[i, j - 1];
          }
          else if (tetha >= 22.5 && tetha < 67.5)
          {
            val1 = _gSobel[i + 1, j - 1];
            val2 = _gSobel[i - 1, j + 1];
          }
          else if (tetha >= 67.5 && tetha < 112.5)
          {
            val1 = _gSobel[i + 1, j];
            val2 = _gSobel[i - 1, j];
          }
          else if (tetha >= 112.5 && tetha < 157.5)
          {
            val1 = _gSobel[i - 1, j - 1];
            val2 = _gSobel[i + 1, j + 1];
          }

          if (_gSobel[i, j] >= val1 && _gSobel[i, j] >= val2)
          {
            newPixels[i, j] = pixel;
          }
          else
          {
            newPixels[i, j] = 0;
          }
        }
      }

      return newPixels;
    }

    #endregion Public Methods

    #region Private Methods

    private static uint[,] BindingEdges(int width, int height, uint[,] pixels)
    {
      var newPixels = new uint[height, width];

      var maxLimit = 150;
      var minLimit = 100;

      var max = RGB2Pixel(new RGB_f { R = maxLimit, G = maxLimit, B = maxLimit });
      var min = RGB2Pixel(new RGB_f { R = minLimit, G = minLimit, B = minLimit });

      var whitePixel = RGB2Pixel(new RGB_f { R = 255, G = 255, B = 255 });
      var middlePixel = RGB2Pixel(new RGB_f { R = 127, G = 127, B = 127 });

      for (int i = 1; i < height - 1; i++)
      {
        for (int j = 1; j < width - 1; j++)
        {
          if (pixels[i, j] >= max)
          {
            newPixels[i, j] = whitePixel;
          }
          else if (pixels[i, j] > min)
          {
            newPixels[i, j] = middlePixel;
          }
          else
          {
            newPixels[i, j] = 0;
          }
        }
      }

      for (int i = 1; i < height - 1; i++)
      {
        for (int j = 1; j < width - 1; j++)
        {
          if (newPixels[i, j] == middlePixel)
          {
            if (newPixels[i - 1, j - 1] == whitePixel || newPixels[i - 1, j] == whitePixel ||
                newPixels[i - 1, j + 1] == whitePixel || newPixels[i, j - 1] == whitePixel ||
                newPixels[i, j + 1] == whitePixel || newPixels[i + 1, j - 1] == whitePixel ||
                newPixels[i + 1, j] == whitePixel || newPixels[i + 1, j + 1] == whitePixel)
            {
              newPixels[i, j] = whitePixel;
            }
          }
        }
      }

      return newPixels;
    }

    private static uint[,] GetExpandedPixelArray(uint[,] pixels, int gap, int width, int height, int tmpWidth, int tmpHeight)
    {
      int i, j;
      uint[,] tmpPixels = new uint[tmpHeight, tmpWidth];

      //заполнение временного расширенного изображения
      //углы
      for (i = 0; i < gap; i++)
      {
        for (j = 0; j < gap; j++)
        {
          tmpPixels[i, j] = pixels[0, 0];
          tmpPixels[i, tmpWidth - 1 - j] = pixels[0, width - 1];
          tmpPixels[tmpHeight - 1 - i, j] = pixels[height - 1, 0];
          tmpPixels[tmpHeight - 1 - i, tmpWidth - 1 - j] = pixels[height - 1, width - 1];
        }
      }

      //крайние левая и правая стороны
      for (i = gap; i < tmpHeight - gap; i++)
      {
        for (j = 0; j < gap; j++)
        {
          tmpPixels[i, j] = pixels[i - gap, j];
          tmpPixels[i, tmpWidth - 1 - j] = pixels[i - gap, width - 1 - j];
        }
      }

      //крайние верхняя и нижняя стороны
      for (i = 0; i < gap; i++)
      {
        for (j = gap; j < tmpWidth - gap; j++)
        {
          tmpPixels[i, j] = pixels[i, j - gap];
          tmpPixels[tmpHeight - 1 - i, j] = pixels[height - 1 - i, j - gap];
        }
      }

      //центр
      for (i = 0; i < height; i++)
      {
        for (j = 0; j < width; j++)
        {
          tmpPixels[i + gap, j + gap] = pixels[i, j];
        }
      }

      return tmpPixels;
    }

    private static RGB_f GetNewPixel(uint[,] pixels, double[,] matrix, int N, int gap, int i, int j)
    {
      var newPixel = new RGB_f { R = 0, G = 0, B = 0 };

      for (int k = 0; k < N; k++)
      {
        for (int m = 0; m < N; m++)
        {
          var newPixelOfCell = CalculateNewColor(pixels[i - gap + k, j - gap + m], matrix[k, m]);
          newPixel.R += newPixelOfCell.R;
          newPixel.G += newPixelOfCell.G;
          newPixel.B += newPixelOfCell.B;
        }
      }

      return newPixel;
    }

    private static bool IsDiag1(double angle)
    {
      return ((angle >= -7 * Math.PI / 8) && (angle < -5 * Math.PI / 8))
              || ((angle >= Math.PI / 8) && (angle < 3 * Math.PI / 8));
    }

    private static bool IsDiag2(double angle)
    {
      return ((angle >= -3 * Math.PI / 8) && (angle < -(Math.PI / 8)))
              || ((angle >= 5 * Math.PI / 8) && (angle < 7 * Math.PI / 8));
    }

    private static bool IsHorizonatal(double angle)
    {
      return (angle < -7 * Math.PI / 8) || ((angle >= -(Math.PI / 8))
              && (angle < Math.PI / 8)) || (angle >= 7 * Math.PI / 8);
    }

    private static bool IsVertical(double angle)
    {
      return ((angle >= -5 * Math.PI / 8) && (angle < -3 * Math.PI / 8))
              || ((angle >= 3 * Math.PI / 8) && (angle < 5 * Math.PI / 8));
    }

    private static void NormalizedMatrix(double[,] matrix, int n)
    {
      var div = 0.0;
      for (int i = 0; i < n; i++)
      {
        for (int j = 0; j < n; j++)
        {
          div += matrix[i, j];
        }
      }

      for (int i = 0; i < n; i++)
      {
        for (int j = 0; j < n; j++)
        {
          matrix[i, j] /= div;
        }
      }
    }

    private static void ValidatePixelColors(RGB_f color)
    {
      if (color.R < 0)
      { color.R = 0; }
      if (color.R > 255)
      { color.R = 255; }
      if (color.G < 0)
      { color.G = 0; }
      if (color.G > 255)
      { color.G = 255; }
      if (color.B < 0)
      { color.B = 0; }
      if (color.B > 255)
      { color.B = 255; }
    }

    #endregion Private Methods
  }
}
