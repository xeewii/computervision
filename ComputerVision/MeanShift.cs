﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ComputerVision
{
  internal class KernelCell
  {
    #region Public Fields

    public uint B;
    public uint G;
    public uint R;
    public double WindowSize;

    #endregion Public Fields

    #region Public Constructors

    public KernelCell(uint r, uint g, uint b, double windowSize)
    {
      R = r;
      G = g;
      B = b;
      WindowSize = windowSize;
    }

    #endregion Public Constructors

    #region Public Methods

    public double Distance(uint r1, uint g1, uint b1, uint r2, uint g2, uint b2)
    {
      return (double) (r1 - r2) * (r1 - r2) +
             (double) (g1 - g2) * (g1 - g2) +
             (double) (b1 - b2) * (b1 - b2);
    }

    public bool InWindow(uint r, uint g, uint b)
    {
      return WindowSize * WindowSize > Distance(r, g, b, R, G, B);
    }

    #endregion Public Methods
  }

  internal class MeanShift
  {
    #region Public Fields

    public static readonly int MaxCountIterations = 10;
    public static readonly int NumberClusters = 10;
    public static readonly double WindowSize = 20;

    #endregion Public Fields

    #region Private Fields

    private static readonly Random Random = new Random();
    private static int _length;

    #endregion Private Fields

    #region Public Methods

    public static uint[,] Execute(int width, int height, uint[,] pixels)
    {
      var newPixels = new uint[height, width];

      _length = height * width;

      var red = new uint[_length];
      var green = new uint[_length];
      var blue = new uint[_length];

      for (var i = 0; i < height - 1; i++)
      {
        for (var j = 0; j < width; j++)
        {
          var rgb = Filters.Pixel2RGB(pixels[i, j]);

          red[i * (height - 1) + j] = (uint) rgb.R;
          green[i * (height - 1) + j] = (uint) rgb.G;
          blue[i * (height - 1) + j] = (uint) rgb.B;
        }
      }

      var orphans = new bool[_length];

      for (var i = 0; i < _length - 1; i++)
      {
        orphans[i] = true;
      }

      var kernels = new List<KernelCell>();
      for (var i = 0; i < NumberClusters && HasOrphans(orphans); i++)
      {
        kernels.Add(GetNewKernelCell(red, green, blue, orphans));
      }

      for (var i = 0; i < _length - 1; i++)
      {
        var closestKernelCell = kernels[0];
        var minDist = double.MaxValue;
        foreach (var kernel in kernels)
        {
          var dist = kernel.Distance(kernel.R, kernel.G, kernel.B, red[i], green[i], blue[i]);

          if (minDist > dist)
          {
            closestKernelCell = kernel;
            minDist = dist;
          }
        }

        red[i] = closestKernelCell.R;
        green[i] = closestKernelCell.G;
        blue[i] = closestKernelCell.B;
      }

      for (var i = 0; i < height - 1; i++)
      {
        for (var j = 0; j < width; j++)
        {
          newPixels[i, j] = Filters.RGB2Pixel(new RGB_f { R = red[i * (height - 1) + j], G = green[i * (height - 1) + j], B = blue[i * (height - 1) + j] });
        }
      }

      return newPixels;
    }

    #endregion Public Methods

    #region Private Methods

    private static KernelCell GetNewKernelCell(IReadOnlyList<uint> red, IReadOnlyList<uint> green, IReadOnlyList<uint> blue, bool[] orphans)
    {
      var kernel = CreateKernel(red, green, blue, orphans);
      var i = 0;
      bool hasChanged;

      do
      {
        hasChanged = Step(red, green, blue, orphans, kernel);
        i++;
      } while (hasChanged && i < MaxCountIterations);

      for (var k = 0; k < _length - 1; k++)
      {
        if (kernel.InWindow(red[k], green[k], blue[k]))
        {
          orphans[k] = false;
        }
      }

      return kernel;
    }

    private static KernelCell CreateKernel(IReadOnlyList<uint> red, IReadOnlyList<uint> green, IReadOnlyList<uint> blue, IEnumerable<bool> orphans)
    {
      var size = orphans.Count(orphan => orphan);

      var i = Random.Next(size);
      return new KernelCell(red[i], green[i], blue[i], WindowSize);
    }

    private static bool HasOrphans(IEnumerable<bool> orphans)
    {
      return orphans.Any(orphan => orphan);
    }

    private static bool Step(IReadOnlyList<uint> red, IReadOnlyList<uint> green, IReadOnlyList<uint> blue, IReadOnlyList<bool> orphans, KernelCell kernelCell)
    {
      uint redSum = 0;
      uint greenSum = 0;
      uint blueSum = 0;
      var amount = 0;

      for (var i = 0; i < _length - 1; i++)
      {
        var r = red[i];
        var g = green[i];
        var b = blue[i];

        if (!kernelCell.InWindow(r, g, b) || !orphans[i])
        {
          continue;
        }

        redSum += r;
        greenSum += g;
        blueSum += b;

        amount++;
      }

      if (amount == 0)
      {
        return false;
      }

      var newRedCenter = (uint) Math.Round((double) redSum / (double) amount);
      var newGreenCenter = (uint) Math.Round((double) greenSum / (double) amount);
      var newBlueCenter = (uint) Math.Round((double) blueSum / (double) amount);

      if (kernelCell.R == newRedCenter && kernelCell.G == newGreenCenter && kernelCell.B == newBlueCenter)
      {
        return false;
      }

      kernelCell.R = newRedCenter;
      kernelCell.G = newGreenCenter;
      kernelCell.B = newBlueCenter;

      return true;
    }

    #endregion Private Methods
  }
}
