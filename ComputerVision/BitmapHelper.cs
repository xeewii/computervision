﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace ComputerVision
{
  internal class BitmapHelper
  {
    #region Public Methods

    public static BitmapImage Bitmap2BitmapImage(Bitmap bitmap)
    {
      MemoryStream ms = new MemoryStream();
      bitmap.Save(ms, ImageFormat.Bmp);
      BitmapImage image = new BitmapImage();
      image.BeginInit();
      ms.Seek(0, SeekOrigin.Begin);
      image.StreamSource = ms;
      image.EndInit();
      return image;
    }

    public static uint[,] Bitmap2PixelArray(Bitmap bitmap)
    {
      var pixels = new uint[bitmap.Height, bitmap.Width];

      for (int y = 0; y < bitmap.Height; y++)
      {
        for (int x = 0; x < bitmap.Width; x++)
        {
          pixels[y, x] = (uint) (bitmap.GetPixel(x, y).ToArgb());
        }
      }

      return pixels;
    }

    public static Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
    {
      using (MemoryStream outStream = new MemoryStream())
      {
        BitmapEncoder enc = new BmpBitmapEncoder();
        enc.Frames.Add(BitmapFrame.Create(bitmapImage));
        enc.Save(outStream);
        Bitmap bitmap = new Bitmap(outStream);

        return new Bitmap(bitmap);
      }
    }

    public static Bitmap PixelArray2Bitmap(uint[,] pixels, int width, int height)
    {
      Bitmap bitmap = new Bitmap(width, height);

      for (int y = 0; y < height; y++)
        for (int x = 0; x < width; x++)
          bitmap.SetPixel(x, y, Color.FromArgb((int) pixels[y, x]));

      return bitmap;
    }

    public static BitmapImage PixelArray2BitmapImage(uint[,] pixels, int width, int height)
    {
      var newBitmap = PixelArray2Bitmap(pixels, width, height);

      return Bitmap2BitmapImage(newBitmap);
    }

    public static BitmapImage Writeable2BitmapImage(WriteableBitmap wbm)
    {
      BitmapImage bmImage = new BitmapImage();
      using (MemoryStream stream = new MemoryStream())
      {
        PngBitmapEncoder encoder = new PngBitmapEncoder();
        encoder.Frames.Add(BitmapFrame.Create(wbm));
        encoder.Save(stream);
        bmImage.BeginInit();
        bmImage.CacheOption = BitmapCacheOption.OnLoad;
        bmImage.StreamSource = stream;
        bmImage.EndInit();
        bmImage.Freeze();
      }
      return bmImage;
    }

    #endregion Public Methods
  }
}
