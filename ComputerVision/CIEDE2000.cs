﻿using System;

namespace ComputerVision
{
  class CIEDE2000
  {
    private const double eps = 0.000000001;
    private const double k_L = 1;
    private const double k_C = 1;
    private const double k_H = 1;
    public static double Execute(LAB lab1, LAB lab2)
    {
      // http://www2.ece.rochester.edu/~gsharma/ciede2000/ciede2000noteCRNA.pdf

      double L1_z = lab1.L;
      double a1_z = lab1.A;
      double b1_z = lab1.B;

      double L2_z = lab2.L;
      double a2_z = lab2.A;
      double b2_z = lab2.B;

      var C1_z = Math.Sqrt(a1_z * a1_z + b1_z * b1_z);
      var C2_z = Math.Sqrt(a2_z * a2_z + b2_z * b2_z);

      var C_z_podch = (C1_z + C2_z) / 2;

      var C_z_podch_pow7 = Math.Pow(C_z_podch, 7);
      var G = 0.5 * (1 - Math.Sqrt(C_z_podch_pow7 / (C_z_podch_pow7 + 6103515625)));

      var a1_apos = (1 + G) * a1_z;
      var a2_apos = (1 + G) * a2_z;

      var C1_apos = Math.Sqrt(a1_apos * a1_apos + b1_z * b1_z);
      var C2_apos = Math.Sqrt(a2_apos * a2_apos + b2_z * b2_z);

      var h1_apos = 0.0;
      var h2_apos = 0.0;

      if (!(b1_z == 0 && a1_apos == 0))
      {
        h1_apos = Math.Atan2(b1_z, a1_apos);
      }

      if (!(b2_z == 0 && a2_apos == 0))
      {
        h1_apos = Math.Atan2(b2_z, a2_apos);
      }

      var L_delta_apos = L2_z - L1_z;
      var C_delta_apos = C2_apos - C1_apos;

      var h_delta_apos = 0.0;
      if (Math.Abs(C1_apos * C2_apos) < eps)
      {
        if (Math.Abs(h2_apos - h1_apos) <= 180)
        {
          h_delta_apos = h2_apos - h1_apos;
        }
        else
        {
          if(h2_apos - h1_apos > 180)
          {
            h_delta_apos = h2_apos - h1_apos - 360;
          }
          else
          {
            if (h2_apos - h1_apos < -180)
            {
              h_delta_apos = h2_apos - h1_apos + 360;
            }
          }
        }
      }
      
      var H_delta_apos = 2 * Math.Sqrt(C1_apos * C2_apos) * Math.Sin(h_delta_apos * 0.5);

      var L_apos_podch = 0.5 * (L1_z + L2_z);
      var C_apos_podch = 0.5 * (C1_apos + C2_apos);

      var h_apos_podch = 0.0;
      if (Math.Abs(C1_apos * C2_apos) < eps)
      {
        h_apos_podch = h1_apos + h2_apos;
      }
      else
      {
        if (Math.Abs(h1_apos - h2_apos) <= 180)
        {
          h_apos_podch = 0.5 * (h1_apos + h2_apos);
        }
        else
        {
          if (Math.Abs(h1_apos - h2_apos) > 180 && h1_apos + h2_apos < 360)
          {
            h_apos_podch = (h1_apos + h2_apos + 360) * 0.5;
          }
          else
          {
            h_apos_podch = (h1_apos + h2_apos - 360) * 0.5;
          }
        }
      }

      var T = 1 - 0.17 * Math.Cos(h_apos_podch - 30) + 0.24 * Math.Cos(2 * h_apos_podch) + 0.32*Math.Cos(3*h_apos_podch + 6) - 0.2*Math.Cos(4*h_apos_podch - 63);

      var tao_delta = 30 * Math.Exp(-(int)((h_apos_podch - 275)* (h_apos_podch - 275) / 625));

      var C_apos_podch_pow7 = Math.Pow(C_apos_podch, 7);
      var R_C = 2 * Math.Sqrt(C_apos_podch_pow7 / (C_apos_podch_pow7 + 6103515625));

      var L_apos_podch_minus50_pow2 = (L_apos_podch - 50) * (L_apos_podch - 50);
      var S_L = 1 + 0.015 * L_apos_podch_minus50_pow2 / Math.Sqrt(20 + L_apos_podch_minus50_pow2);

      var S_C = 1 + 0.045 * C_apos_podch;

      var S_H = 1 + 0.015 * C_apos_podch * T;

      var R_T = -Math.Sin(2 * tao_delta) * R_C;

      var E_delta = Math.Sqrt(L_delta_apos * L_delta_apos / (k_L * S_L * k_L * S_L) + C_delta_apos * C_delta_apos / (k_C * S_C * k_C * S_C) + H_delta_apos * H_delta_apos / (k_H * S_H * k_H * S_H) + R_T*C_delta_apos*H_delta_apos / (k_C * S_C * k_H * S_H));

      return E_delta;
    }
  }
}
