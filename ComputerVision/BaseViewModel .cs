﻿using System.ComponentModel;

namespace ComputerVision
{
  public class BaseViewModel : INotifyPropertyChanged
  {
    #region Public Events

    public event PropertyChangedEventHandler PropertyChanged;

    #endregion Public Events

    #region Protected Methods

    protected virtual void OnPropertyChanged(string propertyName = "")
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion Protected Methods
  }
}
