﻿namespace ComputerVision
{
  internal class QuadTree
  {
    #region Public Constructors

    public QuadTree(BoundingBox box, QuadTree[] childs = null)
    {
      Box = box;
      Childs = childs;
    }

    #endregion Public Constructors

    #region Public Properties

    public BoundingBox Box { get; }

    public QuadTree[] Childs { get; set; }

    #endregion Public Properties

    #region Public Methods

    public HSV MeanHSV(uint[,] pixels)
    {
      var h = 0.0;
      var s = 0.0;
      var v = 0.0;

      var count = 0;

      for (var i = Box.StartY; i < Box.StartY + Box.Height; i++)
      {
        for (var j = Box.StartX; j < Box.StartX + Box.Width; j++)
        {
          var rgb = Filters.Pixel2RGB(pixels[i, j]);

          var hsv = Converter.RGBtoHSB((int) rgb.R, (int) rgb.G, (int) rgb.B);

          h += hsv.H;
          s += hsv.S;
          v += hsv.V;

          count++;
        }
      }

      return new HSV { H = h / count, S = s / count, V = v / count };
    }

    #endregion Public Methods
  }
}
