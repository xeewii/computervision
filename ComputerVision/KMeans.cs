﻿using System;
using System.Collections.Generic;

namespace ComputerVision
{
  internal class KMeans
  {
    #region Private Fields

    private const int NumberClusters = 3;
    private static readonly Random Random = new Random();
    private static int _length;

    #endregion Private Fields

    #region Public Methods

    public static uint[,] Execute(int width, int height, uint[,] pixels)
    {
      var newPixels = new uint[height, width];

      _length = height * width;

      var red = new uint[_length];
      var green = new uint[_length];
      var blue = new uint[_length];

      for (var i = 0; i < height - 1; i++)
      {
        for (var j = 0; j < width; j++)
        {
          var rgb = Filters.Pixel2RGB(pixels[i, j]);

          red[i * (height - 1) + j] = (uint) rgb.R;
          green[i * (height - 1) + j] = (uint) rgb.G;
          blue[i * (height - 1) + j] = (uint) rgb.B;
        }
      }

      var clusters = new RGB[NumberClusters];

      for (var i = 0; i < NumberClusters; i++)
      {
        clusters[i] = new RGB((uint) Math.Min(255, Math.Max(0, Random.Next())),
                (uint) Math.Min(255, Math.Max(0, Random.Next())),
                (uint) Math.Min(255, Math.Max(0, Random.Next())));
      }

      Make(red, green, blue, clusters);

      for (var i = 0; i < _length - 1; i++)
      {
        RGB nearestCluster = null;
        var minDist = double.MaxValue;

        foreach (var cluster in clusters)
        {
          var dist = Distance(cluster, red[i], green[i], blue[i]);

          if (minDist > dist)
          {
            nearestCluster = cluster;
            minDist = dist;
          }
        }

        red[i] = nearestCluster.R;
        green[i] = nearestCluster.G;
        blue[i] = nearestCluster.B;
      }

      for (var i = 0; i < height - 1; i++)
      {
        for (var j = 0; j < width; j++)
        {
          newPixels[i, j] = Filters.RGB2Pixel(new RGB_f { R = red[i * (height - 1) + j], G = green[i * (height - 1) + j], B = blue[i * (height - 1) + j] });
        }
      }

      return newPixels;
    }

    #endregion Public Methods

    #region Private Methods

    private static double Distance(RGB rgb, uint r, uint g, uint b)
    {
      return (double) (rgb.R - r) * (rgb.R - r) +
              (double) (rgb.G - g) * (rgb.G - g) +
              (double) (rgb.B - b) * (rgb.B - b);
    }

    private static void Make(IReadOnlyList<uint> red, IReadOnlyList<uint> green, IReadOnlyList<uint> blue, IReadOnlyList<RGB> clusters)
    {
      var reds = new double[NumberClusters];
      var greens = new double[NumberClusters];
      var blues = new double[NumberClusters];
      var masses = new double[NumberClusters];

      bool isOn;

      do
      {
        isOn = false;
        for (var i = 0; i < NumberClusters; i++)
        {
          reds[i] = 0;
          greens[i] = 0;
          blues[i] = 0;
          masses[i] = 0;
        }

        for (var j = 0; j < _length - 1; j++)
        {
          var minDist = double.MaxValue;
          var minNum = 0;

          var r = red[j];
          var g = green[j];
          var b = blue[j];

          for (var i = 0; i < NumberClusters - 1; i++)
          {
            var dist = Distance(clusters[i], r, g, b);

            if (minDist > dist)
            {
              minDist = dist;
              minNum = i;
            }
          }

          reds[minNum] += r;
          greens[minNum] += g;
          blues[minNum] += b;
          masses[minNum]++;
        }

        for (var i = 0; i < NumberClusters; i++)
        {
          var newRed = (uint) Math.Round(reds[i] / masses[i]);
          var newGreen = (uint) Math.Round(greens[i] / masses[i]);
          var newBlue = (uint) Math.Round(blues[i] / masses[i]);

          if (newRed == clusters[i].R && newGreen == clusters[i].G && newBlue == clusters[i].B)
          {
            continue;
          }

          isOn = true;
          clusters[i].R = newRed;
          clusters[i].G = newGreen;
          clusters[i].B = newBlue;
        }
      } while (isOn);
    }

    #endregion Private Methods

    #region Private Classes

    private class RGB
    {
      #region Public Fields

      public uint B;
      public uint G;
      public uint R;

      #endregion Public Fields

      #region Public Constructors

      public RGB(uint r, uint g, uint b)
      {
        R = r;
        G = g;
        B = b;
      }

      #endregion Public Constructors
    }

    #endregion Private Classes
  }
}
