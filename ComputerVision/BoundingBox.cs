﻿namespace ComputerVision
{
  internal class BoundingBox
  {
    #region Public Constructors

    public BoundingBox(int startX, int startY, int height, int width)
    {
      StartX = startX;
      StartY = startY;
      Height = height;
      Width = width;
    }

    #endregion Public Constructors

    #region Public Properties

    public int Height { get; }

    public int StartX { get; }

    public int StartY { get; }

    public int Width { get; }

    #endregion Public Properties
  }
}
