﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputerVision
{
  class NormalizedCutSegmentation
  {
    private static uint[,] _pixels;

    public static uint[,] Execute(int width, int height, uint[,] pixels)
    {
      _pixels = new uint[height, width];
      for (var i = 0; i < height; i++)
      {
        for (var j = 0; j < width; j++)
        {
          _pixels[i, j] = pixels[i, j];
        }
      }

      var countPixels = width * height;
      var W = new double[countPixels, countPixels];
      for (var i = 0; i < height; i++)
      {
        for (var j = 0; j < width; j++)
        { 
          LAB lab_ij = Converter.RGBtoLab(Filters.Pixel2RGB(_pixels[i, j]));
          for (var m = 0; m < height; m++)
          {
            for (var n = 0; n < width; n++)
            {
              LAB lab_mn = Converter.RGBtoLab(Filters.Pixel2RGB(_pixels[m, n]));

              W[i * height + j, m* height + n] = CIEDE2000.Execute(lab_ij, lab_mn);
            }
          }
        }
      }

      var D = new double[countPixels, countPixels];
      for (var i = 0; i < countPixels; i++)
      {
        var sum = 0.0;
        for (var j = 0; j < countPixels; j++)
        {
          sum += W[i, j];
        }

        D[i, i] = sum;
      }




      return _pixels;
    }
  }
}
