﻿using System.Runtime.InteropServices;

namespace ComputerVision
{
  public struct HSV
  {
    #region Public Fields

    public double H; // [0,360]
    public double S; // [0,100]
    public double V;

    #endregion Public Fields

    // [0,100]
  }

  public struct LAB
  {
    #region Public Fields

    public int A;
    public int B;
    public int L;

    #endregion Public Fields
  }

  [StructLayout(LayoutKind.Explicit)]
  public struct RGB
  {
    [FieldOffset(0)]
    public byte B;

    [FieldOffset(1)]
    public byte G;

    [FieldOffset(2)]
    public byte R;

    [FieldOffset(3)]
    public byte A;
  }

  public struct RGB_f
  {
    #region Public Fields

    public float B;
    public float G;
    public float R;

    #endregion Public Fields
  }

  public struct XYZ
  {
    #region Public Fields

    public double X;
    public double Y;
    public double Z;

    #endregion Public Fields
  }

  public struct YIQ
  {
    #region Public Fields

    public double I;
    public double Q;
    public double Y;

    #endregion Public Fields
  }
}
