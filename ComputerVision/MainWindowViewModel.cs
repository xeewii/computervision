﻿using Microsoft.Win32;
using System;
using System.Drawing;
using System.Windows.Media.Imaging;

namespace ComputerVision
{
  public class MainWindowViewModel : BaseViewModel
  {
    #region Private Fields

    private const int SigmaGaussianDefault = 10;

    private Bitmap _bitmap;
    private BitmapImage _bitmapImage;
    private Bitmap _bitmapOriginal;
    private string _hsvText;
    private bool _isGaborFilterEnabled;
    private bool _isGaussianFilterEnabled;
    private bool _isKannyFilterEnabled;
    private bool _isOtsuFilterEnabled;
    private bool _isSobelFilterEnabled;
    private bool _isSplitAndMergeEnabled;
    private bool _isKMeansEnabled;
    private bool _isMeanShiftEnabled;
    private string _labText;
    private double[] _lComponentValues;
    private uint[,] _pixels;
    private string _rgbText;
    private int _sigmaGaussian;

    #endregion Private Fields

    #region Public Constructors

    public MainWindowViewModel()
    {
      LComponentValues = new double[101];
      var fileName = @"Images/cat.png";
      _bitmapOriginal = new Bitmap(fileName);
      BitmapImage = BitmapHelper.Bitmap2BitmapImage(_bitmapOriginal);

      _pixels = BitmapHelper.Bitmap2PixelArray(_bitmapOriginal);

      SigmaGaussian = SigmaGaussianDefault;

      InitLComponentValues();
    }

    #endregion Public Constructors

    #region Public Properties

    public BitmapImage BitmapImage
    {
      get { return _bitmapImage; }
      set
      {
        _bitmapImage = value;
        _bitmap = BitmapHelper.BitmapImage2Bitmap(value);

        InitLComponentValues();

        OnPropertyChanged(nameof(BitmapImage));
      }
    }

    public string HSVText
    {
      get { return _hsvText; }
      set
      {
        _hsvText = value;

        OnPropertyChanged(nameof(HSVText));
      }
    }

    public bool IsGaborFilterEnabled
    {
      get { return _isGaborFilterEnabled; }
      set
      {
        _isGaborFilterEnabled = value;

        OnIsGaborFilterEnabledChanged();
      }
    }

    public bool IsGaussianFilterEnabled
    {
      get { return _isGaussianFilterEnabled; }
      set
      {
        _isGaussianFilterEnabled = value;

        OnIsGaussianFilterEnabledChanged();
      }
    }

    public bool IsKannyFilterEnabled
    {
      get { return _isKannyFilterEnabled; }
      set
      {
        _isKannyFilterEnabled = value;

        OnIsKannyFilterEnabledChanged();
      }
    }

    public bool IsOtsuFilterEnabled
    {
      get { return _isOtsuFilterEnabled; }
      set
      {
        _isOtsuFilterEnabled = value;

        OnIsOtsuFilterEnabledChanged();
      }
    }

    public bool IsSobelFilterEnabled
    {
      get { return _isSobelFilterEnabled; }
      set
      {
        _isSobelFilterEnabled = value;

        OnIsSobelFilterEnabledChanged();
      }
    }

    public bool IsSplitAndMergeEnabled
    {
      get { return _isSplitAndMergeEnabled; }
      set
      {
        _isSplitAndMergeEnabled = value;

        OnIsSplitAndMergeEnabledChanged();
      }
    }

    public bool IsKMeansEnabled
    {
      get { return _isKMeansEnabled; }
      set
      {
        _isKMeansEnabled = value;

        OnIsKMeansEnabledChanged();
      }
    }

    public bool IsMeanShiftEnabled
    {
      get { return _isMeanShiftEnabled; }
      set
      {
        _isMeanShiftEnabled = value;

        OnIsMeanShiftEnabledChanged();
      }
    }

    public string LabText
    {
      get { return _labText; }
      set
      {
        _labText = value;

        OnPropertyChanged(nameof(LabText));
      }
    }

    public double[] LComponentValues
    {
      get { return _lComponentValues; }
      set
      {
        _lComponentValues = value;

        OnPropertyChanged(nameof(LComponentValues));
      }
    }

    public string RGBText
    {
      get { return _rgbText; }
      set
      {
        _rgbText = value;

        OnPropertyChanged(nameof(RGBText));
      }
    }

    public int SigmaGaussian
    {
      get { return _sigmaGaussian; }
      set
      {
        _sigmaGaussian = value;

        OnPropertyChanged(nameof(SigmaGaussian));
      }
    }

    #endregion Public Properties

    #region Public Methods

    public void ChangeHighlightPixelInfo(RGB rgb)
    {
      RGBText = "R = " + rgb.R + " G = " + rgb.G + " B = " + rgb.B;

      var hsv = Converter.RGB2HSV(rgb);

      HSVText = "H = " + (int) hsv.H + " S = " + (int) hsv.S + " V = " + (int) hsv.V;

      var lab = Converter.RGB2LAB(rgb);

      LabText = "L = " + lab.L + " a = " + lab.A + " b = " + lab.B;
    }

    public unsafe void ChangeHSV(HSV hsv)
    {
      var bitmap = new WriteableBitmap(BitmapHelper.Bitmap2BitmapImage(_bitmapOriginal));
      var data = (RGB*) bitmap.BackBuffer;
      var stride = bitmap.BackBufferStride / 4;
      bitmap.Lock();

      for (int i = 0; i < bitmap.Height; ++i)
      {
        for (int j = 0; j < bitmap.Width; ++j)
        {
          RGB rgb = (*(data + i * stride + j));
          rgb = Editor.ChangeHSV(rgb, hsv);
          (*(data + i * stride + j)) = rgb;
        }
      }

      bitmap.Unlock();
      BitmapImage = BitmapHelper.Writeable2BitmapImage(bitmap);

      /*_bitmap = new Bitmap(_originalBmp);
      Rectangle rect = new Rectangle(0, 0, _bitmap.Width, _bitmap.Height);
      _bmpData = _bitmap.LockBits(rect, ImageLockMode.ReadWrite, _bitmap.PixelFormat);
      var ptr = _bmpData.Scan0;
      var bytes = Math.Abs(_bmpData.Stride) * _bitmap.Height;
      var rgbValues = new byte[bytes];
      System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

      for (int i = 0; i < rgbValues.Length; i++)
      {
        int color = rgbValues[i];
        byte b = (byte) ((color) & 0xff);
        byte g = (byte) ((color >> 8) & 0xff);
        byte r = (byte) ((color >> 16) & 0xff);
        var rgb = new RGB { R = r, G = g, B = b };

        rgb = Editor.TransformHSV(rgb, val, 0, 0);

        byte rgbByte = (byte) (((rgb.R & 0xff) << 16) | ((rgb.G & 0xff) << 8) | ((rgb.B & 0xff)));

        rgbValues[i] = rgbByte;
      }

      System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);
      _bitmap.UnlockBits(_bmpData);
      */
    }

    public void LoadImage()
    {
      OpenFileDialog openFileDialog = new OpenFileDialog();
      if (openFileDialog.ShowDialog() == true)
      {
        var fileName = openFileDialog.FileName;
        Uri fileUri = new Uri(fileName);
        _bitmapOriginal = new Bitmap(fileName);
        BitmapImage = new BitmapImage(fileUri);

        _pixels = BitmapHelper.Bitmap2PixelArray(_bitmapOriginal);

        InitLComponentValues();
      }
    }

    #endregion Public Methods

    #region Private Methods

    private unsafe void InitLComponentValues()
    {
      var bitmap = new WriteableBitmap(BitmapHelper.Bitmap2BitmapImage(_bitmapOriginal));
      var data = (RGB*) bitmap.BackBuffer;
      var stride = bitmap.BackBufferStride / 4;
      bitmap.Lock();

      var lcomponentValues = new double[101];

      for (int i = 0; i < bitmap.Height; ++i)
      {
        for (int j = 0; j < bitmap.Width; ++j)
        {
          RGB rgb = (*(data + i * stride + j));
          LAB lab = Converter.RGB2LAB(rgb);
          lcomponentValues[lab.L]++;
        }
      }

      bitmap.Unlock();

      //нормирование
      var factor = 1500 / (bitmap.Height * bitmap.Width);
      for (int i = 0; i < lcomponentValues.Length; i++)
      {
        lcomponentValues[i] *= factor;
      }

      LComponentValues = lcomponentValues;
    }

    private void OnIsGaborFilterEnabledChanged()
    {
      if (_isGaborFilterEnabled)
      {
        var newPixels = Filters.GaborFilterExecute(_bitmapOriginal.Width, _bitmapOriginal.Height, _pixels);

        BitmapImage = BitmapHelper.PixelArray2BitmapImage(newPixels, _bitmapOriginal.Width, _bitmapOriginal.Height);
      }
      else
      {
        BitmapImage = BitmapHelper.Bitmap2BitmapImage(_bitmapOriginal);
      }
    }

    private void OnIsGaussianFilterEnabledChanged()
    {
      if (_isGaussianFilterEnabled)
      {
        var newPixels = Filters.GaussianFilterExecute(_bitmapOriginal.Width, _bitmapOriginal.Height, _pixels, _sigmaGaussian);

        BitmapImage = BitmapHelper.PixelArray2BitmapImage(newPixels, _bitmapOriginal.Width, _bitmapOriginal.Height);
      }
      else
      {
        BitmapImage = BitmapHelper.Bitmap2BitmapImage(_bitmapOriginal);
      }
    }

    private void OnIsKannyFilterEnabledChanged()
    {
      if (_isKannyFilterEnabled)
      {
        var newPixels = Filters.KannyFilterExecute(_bitmapOriginal.Width, _bitmapOriginal.Height, _pixels);

        BitmapImage = BitmapHelper.PixelArray2BitmapImage(newPixels, _bitmapOriginal.Width, _bitmapOriginal.Height);
      }
      else
      {
        BitmapImage = BitmapHelper.Bitmap2BitmapImage(_bitmapOriginal);
      }
    }

    private void OnIsOtsuFilterEnabledChanged()
    {
      if (_isOtsuFilterEnabled)
      {
        var newPixels = Filters.OtsuFilterExecute(_bitmapOriginal.Width, _bitmapOriginal.Height, _pixels);

        BitmapImage = BitmapHelper.PixelArray2BitmapImage(newPixels, _bitmapOriginal.Width, _bitmapOriginal.Height);
      }
      else
      {
        BitmapImage = BitmapHelper.Bitmap2BitmapImage(_bitmapOriginal);
      }
    }

    private void OnIsSobelFilterEnabledChanged()
    {
      if (_isSobelFilterEnabled)
      {
        var newPixels = Filters.SobelFilterExecute(_bitmapOriginal.Width, _bitmapOriginal.Height, _pixels);

        BitmapImage = BitmapHelper.PixelArray2BitmapImage(newPixels, _bitmapOriginal.Width, _bitmapOriginal.Height);
      }
      else
      {
        BitmapImage = BitmapHelper.Bitmap2BitmapImage(_bitmapOriginal);
      }
    }

    private void OnIsSplitAndMergeEnabledChanged()
    {
      if (_isSplitAndMergeEnabled)
      {
        var newPixels = SplitAndMergeSegmentation.Execute(_bitmapOriginal.Width, _bitmapOriginal.Height, _pixels);

        BitmapImage = BitmapHelper.PixelArray2BitmapImage(newPixels, _bitmapOriginal.Width, _bitmapOriginal.Height);
      }
      else
      {
        BitmapImage = BitmapHelper.Bitmap2BitmapImage(_bitmapOriginal);
      }
    }

    private void OnIsKMeansEnabledChanged()
    {
      if (_isKMeansEnabled)
      {
        var newPixels = KMeans.Execute(_bitmapOriginal.Width, _bitmapOriginal.Height, _pixels);

        BitmapImage = BitmapHelper.PixelArray2BitmapImage(newPixels, _bitmapOriginal.Width, _bitmapOriginal.Height);
      }
      else
      {
        BitmapImage = BitmapHelper.Bitmap2BitmapImage(_bitmapOriginal);
      }
    }

    private void OnIsMeanShiftEnabledChanged()
    {
      if (_isMeanShiftEnabled)
      {
        var newPixels = MeanShift.Execute(_bitmapOriginal.Width, _bitmapOriginal.Height, _pixels);

        BitmapImage = BitmapHelper.PixelArray2BitmapImage(newPixels, _bitmapOriginal.Width, _bitmapOriginal.Height);
      }
      else
      {
        BitmapImage = BitmapHelper.Bitmap2BitmapImage(_bitmapOriginal);
      }
    }
    #endregion Private Methods
  }
}
