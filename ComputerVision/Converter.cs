﻿using System;

namespace ComputerVision
{
  internal class Converter
  {
    #region Private Fields

    private const float epsilon = 0.000001f;

    #endregion Private Fields

    #region Public Methods

    public static RGB HSV2RGB(HSV hsv)
    {
      var s = hsv.S / 100;
      var v = hsv.V / 100;

      double r = 0, g = 0, b = 0; // 0.0-1.0

      int hi = (int) (hsv.H / 60.0f) % 6;
      double f = (hsv.H / 60.0f) - hi;
      double p = v * (1.0f - s);
      double q = v * (1.0f - s * f);
      double t = v * (1.0f - s * (1.0f - f));

      switch (hi)
      {
        case 0:
          { r = v; g = t; b = p; break; }
        case 1:
          {
            r = q;
            g = v;
            b = p;
            break;
          }
        case 2:
          { r = p; g = v; b = t; break; }
        case 3:
          {
            r = p;
            g = q;
            b = v;
            break;
          }
        case 4:
          {
            r = t;
            g = p;
            b = v;
            break;
          }
        case 5:
          {
            r = v;
            g = p;
            b = q;
            break;
          }
      }
      byte res_r, res_g, res_b;
      res_r = (byte) (r * 255); // dst_r : 0-255
      res_g = (byte) (g * 255); // dst_r : 0-255
      res_b = (byte) (b * 255); // dst_r : 0-255

      return new RGB { R = res_r, G = res_g, B = res_b };
    }

    public static LAB RGBtoLab(RGB_f rgb)
    {
      return XYZ2LAB(RGBtoXYZ(rgb.R, rgb.G, rgb.B));
    }

    public static XYZ RGBtoXYZ(double red, double green, double blue)
    {
      double rLinear = (double) red / 255.0;
      double gLinear = (double) green / 255.0;
      double bLinear = (double) blue / 255.0;

      double r = (rLinear > 0.04045) ? Math.Pow((rLinear + 0.055) / (
        1 + 0.055), 2.2) : (rLinear / 12.92);
      double g = (gLinear > 0.04045) ? Math.Pow((gLinear + 0.055) / (
        1 + 0.055), 2.2) : (gLinear / 12.92);
      double b = (bLinear > 0.04045) ? Math.Pow((bLinear + 0.055) / (
        1 + 0.055), 2.2) : (bLinear / 12.92);

      return new XYZ
      {
        X = r * 0.4124 + g * 0.3576 + b * 0.1805,
        Y = r * 0.2126 + g * 0.7152 + b * 0.0722,
        Z = r * 0.0193 + g * 0.1192 + b * 0.9505
      };
    }

    public static RGB_f HSVtoRGB(double h, double s, double v)
    {
      double r = 0;
      double g = 0;
      double b = 0;

      if (s == 0)
      {
        r = g = b = v;
      }
      else
      {
        // цветовой круг состоит из 6 секторов. Выяснить, в каком секторе
        // находится.
        double sectorPos = h / 60.0;
        int sectorNumber = (int) (Math.Floor(sectorPos));
        // получить дробную часть сектора
        double fractionalSector = sectorPos - sectorNumber;

        // вычислить значения для трех осей цвета.
        double p = v * (1.0 - s);
        double q = v * (1.0 - (s * fractionalSector));
        double t = v * (1.0 - (s * (1 - fractionalSector)));

        // присвоить дробные цвета r, g и b на основании сектора
        // угол равняется.
        switch (sectorNumber)
        {
          case 0:
          r = v;
          g = t;
          b = p;
          break;

          case 1:
          r = q;
          g = v;
          b = p;
          break;

          case 2:
          r = p;
          g = v;
          b = t;
          break;

          case 3:
          r = p;
          g = q;
          b = v;
          break;

          case 4:
          r = t;
          g = p;
          b = v;
          break;

          case 5:
          r = v;
          g = p;
          b = q;
          break;
        }
      }

      return new RGB_f
      {
        R = Convert.ToInt32(Double.Parse(String.Format("{0:0.00}", r * 255.0))),
        G = Convert.ToInt32(Double.Parse(String.Format("{0:0.00}", g * 255.0))),
        B = Convert.ToInt32(Double.Parse(String.Format("{0:0.00}", b * 255.0)))
      };
    }

    public static HSV RGB2HSV(RGB rgb)
    {
      float r = rgb.R / 255f;
      float g = rgb.G / 255f;
      float b = rgb.B / 255f;

      var max = Math.Max(r, Math.Max(g, b));
      var min = Math.Min(r, Math.Min(g, b));

      var delta = max - min;

      float h = 0;

      if (delta > epsilon)
      {
        if (max - r < epsilon)
        {
          if (g >= b)
          {
            h = 60 * (g - b) / delta;
          }
          else
          {
            h = 60 * (g - b) / delta + 360;
          }
        }
        else
        {
          if (max - g < epsilon)
          {
            h = 60 * (b - r) / delta + 120;
          }
          else
          {
            if (max - b < epsilon)
            {
              h = 60 * (r - g) / delta + 240;
            }
          }
        }
      }

      float s;
      if (max < epsilon)
      {
        s = 0;
      }
      else
      {
        s = (1 - min / max) * 100;
      }

      var v = max * 100;

      return new HSV
      {
        H = h,
        S = s,
        V = v
      };
    }

    public static HSV RGB2HSV(RGB_f rgb)
    {
      float r = rgb.R / 255f;
      float g = rgb.G / 255f;
      float b = rgb.B / 255f;

      var max = Math.Max(r, Math.Max(g, b));
      var min = Math.Min(r, Math.Min(g, b));

      var delta = max - min;

      float h = 0;

      if (delta > epsilon)
      {
        if (max - r < epsilon)
        {
          if (g >= b)
          {
            h = 60 * (g - b) / delta;
          }
          else
          {
            h = 60 * (g - b) / delta + 360;
          }
        }
        else
        {
          if (max - g < epsilon)
          {
            h = 60 * (b - r) / delta + 120;
          }
          else
          {
            if (max - b < epsilon)
            {
              h = 60 * (r - g) / delta + 240;
            }
          }
        }
      }

      float s;
      if (max < epsilon)
      {
        s = 0;
      }
      else
      {
        s = (1 - min / max) * 100;
      }

      var v = max * 100;

      return new HSV
      {
        H = h,
        S = s,
        V = v
      };
    }

    public static LAB RGB2LAB(RGB rgb)
    {
      return XYZ2LAB(RGB2XYZ(rgb));
    }

    public static XYZ RGB2XYZ(RGB rgb)
    {
      double r = rgb.R / 255.0;
      double g = rgb.G / 255.0;
      double b = rgb.B / 255.0;

      r = r > 0.04045 ? Math.Pow((r + 0.055) / 1.055, 2.2) : r / 12.92;
      g = g > 0.04045 ? Math.Pow((g + 0.055) / 1.055, 2.2) : g / 12.92;
      b = b > 0.04045 ? Math.Pow((b + 0.055) / 1.055, 2.2) : b / 12.92;

      r *= 100;
      g *= 100;
      b *= 100;

      return new XYZ
      {
        X = 0.4124564 * r + 0.3575761 * g + 0.1804375 * b,
        Y = 0.2126729 * r + 0.7151522 * g + 0.0721750 * b,
        Z = 0.0193339 * r + 0.1191920 * g + 0.9503041 * b
      };
    }

    public static YIQ RGB2YIQ(RGB rgb)
    {
      var r = rgb.R;
      var g = rgb.G;
      var b = rgb.B;

      return new YIQ
      {
        Y = 0.299 * r + 0.587 * g + 0.114 * b,
        I = 0.596 * r - 0.274 * g - 0.321 * b,
        Q = 0.211 * r - 0.523 * g + 0.311 * b
      };
    }

    public static HSV RGBtoHSB(int red, int green, int blue)
    {
      // нормализовать значения красного, зеленого и синего
      double r = ((double) red / 255.0);
      double g = ((double) green / 255.0);
      double b = ((double) blue / 255.0);

      // начало преобразования
      double max = Math.Max(r, Math.Max(g, b));
      double min = Math.Min(r, Math.Min(g, b));

      double h = 0.0;
      if (max == r && g >= b)
      {
        h = 60 * (g - b) / (max - min);
      }
      else if (max == r && g < b)
      {
        h = 60 * (g - b) / (max - min) + 360;
      }
      else if (max == g)
      {
        h = 60 * (b - r) / (max - min) + 120;
      }
      else if (max == b)
      {
        h = 60 * (r - g) / (max - min) + 240;
      }

      double s = (max == 0) ? 0.0 : (1.0 - (min / max));

      return new HSV { H = h, S = s, V = (double) max };
    }

    public static LAB XYZ2LAB(XYZ xyz)
    {
      var xn = 95.04;
      var yn = 100.0;
      var zn = 108.88;

      var fY = fForLab(xyz.Y / yn);

      return new LAB
      {
        L = (int) (116.0 * fY - 16),
        A = (int) (500.0 * (fForLab(xyz.X / xn) - fY)),
        B = (int) (200.0 * (fY - fForLab(xyz.Z / zn)))
      };
    }

    public static RGB YIQ2RGB(YIQ yiq)
    {
      var y = yiq.Y;
      var i = yiq.I;
      var q = yiq.Q;

      var r = GetValidValueFromRGB((byte) (y + 0.956 * i + 0.621 * q));
      var g = GetValidValueFromRGB((byte) (y - 0.272 * i - 0.647 * q));
      var b = GetValidValueFromRGB((byte) (y - 1.107 * i + 1.705 * q));

      return new RGB
      {
        R = r,
        G = g,
        B = b
      };
    }

    #endregion Public Methods

    #region Private Methods

    private static double fForLab(double val)
    {
      return (val > 0.008856) ? Math.Pow(val, 0.3333333) : (7.787 * val + 0.137923);
    }

    private static byte GetValidValueFromRGB(byte val)
    {
      if (val < 0)
      { return 0; }
      if (val > 255)
      { return 255; }
      return val;
    }

    #endregion Private Methods
  }
}
